package com.training.playoff;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Playoff {

    private List<Team> teams;

    public Playoff(String... qualifications) {
        this.teams = initQualifications(qualifications);
    }

    private static List<Team> initQualifications(String... qualifications) {
        List<Team> teams = new ArrayList<Team>();
        for (String match_ : qualifications) {
            StringTokenizer tokenizer = new StringTokenizer(match_, " ");
            Team team1 = new Team(tokenizer.nextToken());
            addWinerTeam(teams, team1);
            Team team2 = new Team(tokenizer.nextToken());
            addLooserTeam(teams, team2);
        }
        return teams;
    }

    public String[] layout() {
        return new Qualification(this.teams).print();
    }

    private static void addWinerTeam(List<Team> teams, Team team) {
        if (teams.contains(team)) {
            addPointToTeam(teams, team);
        } else {
            teams.add(team);
            team.addPoint();
        }
    }

    private static void addLooserTeam(List<Team> teams, Team team) {
        if (!teams.contains(team))
            teams.add(team);
    }

    private static void addPointToTeam(List<Team> teams, Team newTeam) {
        for (Team team : teams)
            if (team.equals(newTeam)) {
                team.addPoint();
                break;
            }
    }

}
